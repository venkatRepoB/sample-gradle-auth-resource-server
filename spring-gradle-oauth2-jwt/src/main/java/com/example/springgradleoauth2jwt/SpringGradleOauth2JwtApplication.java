package com.example.springgradleoauth2jwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class SpringGradleOauth2JwtApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringGradleOauth2JwtApplication.class, args);
	}
}
